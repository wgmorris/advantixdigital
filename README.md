# Install
Run `npm install` in the root directory, then `gulp prebuild && gulp` to start the local server.

# @wp_react
The @wp_react folder is the WordPress theme that lives on the server. It's here for reference, but you could use the folder to SFTP in and make changes.

The JSON dump functionality is in sys/js.php

# Deployment
When done with changes on local env, run `gulp deploy` to upload the changes.