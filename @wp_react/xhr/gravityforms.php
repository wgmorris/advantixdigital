<?php
/**
 * Simple file to help add manual entries to a gravity form, so we don't have to deal w/ GFs output.
 */

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: X-Requested-With');
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp/wp-load.php';

// error_reporting(E_ALL);
// ini_set('display_errors', true);

$data = $_REQUEST;

// -----------------------------------------------

$form = GFAPI::get_form($data['form_id']);

# Build our entry
$entry = array();
$regex = '/^input_([0-9])_([0-9])$/';

foreach ($data AS $key => $value):
	if (preg_match($regex, $key))
		$key = preg_replace($regex, '$2', $key);

	$entry[$key] = $value;
endforeach;

$entry['date_created'] = date('Y-m-d G:i');
$entryID = GFAPI::add_entry($entry);

if (is_wp_error($entryID)):
	die(json_encode(array(
		'error' => true,
		'msg' => $entryID->get_error_message(),
		'form' => $form,
	)));

else:
	GFAPI::send_notifications($form, GFAPI::get_entry($entryID));

	die(json_encode(array(
		'error' => false,
		'msg' => end($form['confirmations']),
		'form' => $form,
	)));
endif;