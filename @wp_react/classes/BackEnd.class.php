<?php
/**
 * advantix
 *
 * Controls back-end deliveries
 */

class BackEnd extends BaseTheme {
	protected static $options, $post;

	public function __construct() {
		self::$options = get_option('r');
	}

	protected function _setting(array $tabs) {
		new Settings($this, $tabs);
	}

	protected function _shortcode(array $callback) {
		foreach ($callback AS $key => $cb)
			add_shortcode($key, $cb);
	}

	protected function _menu(array $menu) {
		register_nav_menus($menu);
	}

	protected function _imagesize(array $args) {
		foreach ($args AS $key => $value)
			add_image_size($key, $value[0], $value[1], $value[2]);
	}

	// -----------------------------------------------

	public static function getChildren() {
		$children = null;

		# Blog
		if (DevTests::isBlog())
			$children = self::getCategories();

		# General pages
		else $children = wp_list_pages(array(
			'title_li' => null,
			'child_of' => self::getRootParent(),
			'depth' => 1,
			'echo' => false,
		));

		return $children;
	}

	public static function getCategories() {
		$categories = null;

		$classes = (is_home() || is_page(BLOG)) ? 'current-cat' : null;

		$categories .= '<li class="'. $classes .'"><a href="'. get_permalink(BLOG) .'">View All</a></li>';
		$categories .= wp_list_categories(array(
			'title_li' => null,
			'depth' => 1,
			'echo' => false,
		));

		return $categories;
	}

	public static function getTerms($tax) {
		$categories = null;

		$curTerm = is_tax() ? get_queried_object()->term_id : 0;
		$parent = is_tax() ? get_queried_object()->parent : 0;

		$terms = get_terms($tax, array(
			'parent' => $parent,
			'hide_empty' => false,
		));

		foreach ($terms AS &$t):
			$class = $curTerm === $t->term_id ? 'current-cat' : '';

			$categories .= '
			<li class="'. $class .'">
				<a href="'. get_term_link($t) .'">'
					. $t->name .
				'</a>
			</li>
			';
		endforeach;

		return $categories;
	}

	public static function getMenu($name, $settings = array()) {
		$defaults = array(
			'theme_location' => $name,
			'container' => null,
			'items_wrap' => '%3$s',
		);

		wp_nav_menu(array_merge($settings, $defaults));
	}

	public static function getMenuLabel($name) {
		$locations = get_nav_menu_locations();

		if (isset($locations[$name])) {
			$obj = wp_get_nav_menu_object($locations[$name]);
			return $obj->name;
		}
	}

	public static function getOption($option) {
		return do_shortcode(self::$options[$option]);
	}

	public static function getPostType($type, $settings = array()) {
		$default = array(
			'post_type' => $type,
			'posts_per_page' => -1,
			'orderby' => 'menu_order',
			'order' => 'ASC',
		);

		return new WP_Query(array_merge($default, $settings));
	}

	public static function getRootParent() {
		global $post;
		$parent = $post->ID;

		if ($post->post_parent)	{
			$ancestors = get_post_ancestors($post->ID);
			$root = count($ancestors)-1;
			$parent = $ancestors[$root];
		}

		return $parent;
	}
}