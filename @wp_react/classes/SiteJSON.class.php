<?php
/**
 * advantix
 *
 * Spits out all important site info in a big json object. 
 * Can compartmentalize if needed.
 */

class SiteJSON {
	private 
		$data = array(),
		$blogID = 0,
		$homeID = 0;

	public function __construct() {
		$this->blogID = get_option('page_for_posts');
		$this->homeID = get_option('page_on_front');
	}

	public function buildAll() {
		$this->data = array(
			'site' => array(
				'name' => get_bloginfo('name'),
				'homepage' => $this->homeID,
				'postspage' => $this->blogID,
				'theme_options' => get_option('r'),
				'perpage' => get_option('posts_per_page'),
			),

			'menus' => array(),
			'pages' => array(),
		);

		return $this
			->buildTaxonomies()
			->buildMenus()
			->buildPostTypes()
			->buildGravityForms()
			->data;
	}

	public function buildOnly($type) {
		return $this->_postType($type)->data;
	}

	/**
	 * Taxonomies
	 */
	private function buildTaxonomies() {
		$taxonomies = get_taxonomies();

		unset($taxonomies['nav_menu']);
		unset($taxonomies['link_category']);
		unset($taxonomies['post_format']);

		array_map(array($this, '_tax'), array_keys($taxonomies));

		return $this;
	}

	private function _tax($tax) {
		if ($tax == 'category')
			$this->data[$tax][] = array(
				'title' => 'All',
				'url' => get_permalink($this->blogID),
			);

		foreach (get_terms($tax) AS $term)
			$this->data[$tax][] = array(
				'title' => $term->name,
				'url' => get_term_link($term),
				'slug' => $term->slug,
			);

		return $this;
	}

	/**
	 * Taxonomies
	 */
	private function buildPostTypes() {
		$types = get_post_types();

		unset($types['cfs']);
		unset($types['attachment']);
		unset($types['revision']);
		unset($types['nav_menu_item']);
		// unset($types['post']);

		array_map(array($this, '_postType'), array_keys($types));

		return $this;
	}

	private function _postType($type) {
		$result = BackEnd::getPostType($type);

		if ($result->have_posts())
		while ($result->have_posts()):
			$result->the_post();
			$id = get_the_ID();

			$postData = array(
				'id' => $id,
				'pid' => wp_get_post_parent_id($id),
				'title' => get_the_title(),
				'type' => $type,
				'date' => get_the_date(),
				'content' => apply_filters('the_content', get_the_content()),
				'excerpt' => apply_filters('the_excerpt', get_the_excerpt()),
				'url' => str_replace(get_home_url(), '', get_permalink()),
				'fields' => CFS()->get(),
				'template' => get_page_template_slug(),
				'is_home' => $this->homeID == $id,
				'is_blog' => $this->blogID == $id,
				'author'  => get_the_author(),
				'author_link' => get_the_author_link(),
				'author_avatar' => get_avatar(get_the_author_id(), 32),
				'seo' => array(
					'title' => get_post_meta($id, '_yoast_wpseo_title', true) ?: get_the_title(),
					'desc' => get_post_meta($id, '_yoast_wpseo_metadesc', true) ?: get_the_excerpt(),
					'keywords' => get_post_meta($id, '_yoast_wpseo_metakeywords', true) ?: '',
				),
			);

			$taxonomies = get_post_taxonomies();
			foreach (get_post_taxonomies() AS $tax)
				foreach (get_the_terms($id, $tax) AS $term)
					$postData[$tax][] = $term->name;

			// Setup custom fields if needed
			$fields =& $postData['fields'];

			if ($type === 'team_member'):
				$fields['photo_1'] = wp_get_attachment_image_src($fields['photo_1'], 'team-photo')[0];
				$fields['photo_2'] = wp_get_attachment_image_src($fields['photo_2'], 'team-photo')[0];
			
			elseif ($postData['template'] === 'template-about.php'):
				foreach ($fields['photos'] AS $i => $r)
					$fields['photos'][$i]['image'] = wp_get_attachment_image_src($r['image'], 'team-photo')[0];

			endif;

			$this->data['pages'][] = $postData;
		endwhile; wp_reset_query();

		return $this;
	}

	/**
	 * Menus
	 */
	private function buildMenus() {
		array_map(array($this, '_menu'), wp_get_nav_menus());
		return $this;
	}

	private function _menu($menu) {
		$menuKey = $menu->slug;
		$items = wp_get_nav_menu_items($menu->term_id);

		foreach (get_nav_menu_locations() AS $slug => $id)
			if ($menu->term_id === $id)
				$menuKey = $slug;

		$this->data['menus'][$menuKey]['label'] = $menu->name;
		$this->data['menus'][$menuKey]['links'] = array();

		foreach ($items AS $item):
			$id = $item->object_id;

			$itemData = array(
				'id' => $id,
				'menu_id' => $item->ID,
				'pid' => wp_get_post_parent_id($id),
				'title' => $item->title,
				'url' => $item->url,
			);

			if ($pid = $item->menu_item_parent):
				foreach ($this->data['menus'][$menuKey]['links'] AS &$link):
					if ($link['menu_id'] == $pid)
						$link['children'][] = $itemData;
				endforeach;
			
			else:
				$this->data['menus'][$menuKey]['links'][] = $itemData;

			endif;
		endforeach;

		return $this;
	}

	/**
	 * Gravity forms
	 */
	private function buildGravityForms() {
		if (class_exists('GFAPI'))
			array_map(array($this, '_gform'), GFAPI::get_forms());

		return $this;
	}

	private function _gform($form) {
		$formData = array(
			'id' => $form['id'],
			'title' => $form['title'],
			'desc' => $form['description'],
			'button' => $form['button'],
			'fields' => array(),
			'confirmation' => end($form['confirmations']),
		);

		foreach ($form['fields'] AS $field)
			$formData['fields'][] = array(
				'id' => $field['id'],
				'title' => $field->label,
				'type' => $field->type,
				'choices' => $field->choices ?: array(),
				'name' => "input_{$form['id']}_{$field['id']}",
				'isRequired' => $field->isRequired,
			);

		$this->data['forms'][] = $formData;
	}
}