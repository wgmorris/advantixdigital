<?php
Header('Content-Type: application/javascript');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: X-Requested-With');
require_once $_SERVER['DOCUMENT_ROOT'] . '/wp/wp-load.php';

// -----------------------------------------------

$type = @$_GET['only'] ?: 'site';
$key = $type . '.json.chunk';

//delete_transient($key);

if (!($data = get_transient($key))):
	$site = new SiteJSON();

	if ($type !== 'site')
		$data = $site->buildOnly($type);

	else $data = $site->buildAll();

	set_transient($key, $data, HOUR_IN_SECONDS);
endif;

// -----------------------------------------------

if ($type === 'site')
	echo 'window.data = ' . json_encode($data);

else echo json_encode($data);