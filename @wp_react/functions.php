<?php
/**
 * advantix
 *
 * Main Functions
 */

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS'):
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Headers: X-Requested-With');
	exit;
endif;

require_once 'classes/autoloader.php';
(new BaseTheme())->debug(0)

// -----------------------------------------------

->addImageSizes(array(array(
	'team-photo' => array(242, 325, true),
	'grid-photo' => array(435, 415, true),
)))

->addMenus(array(array(
	'header' => 'Header Menu',
	'mobile' => 'Mobile Menu',
	'footer' => 'Footer Menu',
)))

->addSettings(array(array(
	'General' => array(
		'phone' => array(
			'name' => 'Phone #',
			'type' => 'text',
		),

		'fax' => array(
			'name' => 'Fax #',
			'type' => 'text',
		),

		'address' => array(
			'name' => 'Address',
			'type' => 'textarea',
		),

		'pubemail' => array(
			'name' => 'Public Email',
			'type' => 'text',
		),
	),

	'Footer' => array(
		'blurb' => array(
			'name' => 'Company Blurb',
			'type' => 'textarea',
		),
	),
)))

->addShortcodes(array(array(
	'button' => function($args, $content = '') {
		return '<a href="'. $args['url'] .'" class="btn waves-effect '. $args['style'] .'">'. $content .'</a>';
	},

	'lead' => function($args, $content = '') {
		return '<div class="lead">' . do_shortcode($content) .'</div>';
	},

	'grid' => function($args, $content) {
		return '<div class="grid grid-'. $args['cols'] .'">'. do_shortcode($content) .'</div>';
	},

	'grid_item' => function($args, $content) {
		return '<div class="item">'. do_shortcode($content) .'</div>';
	},

	'num_list' => function($args, $content) {
		return '<div class="num-list">' . do_shortcode($content) . '</div>';
	},

	'num_item' => function($args, $content) {
		return <<<HTML
<div class="num-listitem">
	<header>
		<span class="num"></span>
		<h3>{$args['title']}</h3>
	</header>

	{$content}
</div>
HTML;
	},
)))

// -----------------------------------------------

->render();

// -----------------------------------------------

add_action('save_post',  'refresh_json_cache');
add_action('new_to_publish',  'refresh_json_cache');

function refresh_json_cache() {
	delete_transient('site.json.chunk');
	delete_transient('spost.json.chunk');
}

add_filter('wpseo_enable_xml_sitemap_transient_caching', '__return_false');