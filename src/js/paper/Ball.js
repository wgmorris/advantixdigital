let colours = ['#B55DFD', '#0FD1D8', '#F1A909'],
	dim = 2

function Ball(center, element) {
	this.origin = center
	this.speed = Math.floor(Math.random() * 15) + 10
	this.gravitate_speed = Math.floor(Math.random() * 1000) + 800
	this.scatter_speed = this.speed / 2
	this.direction = (Math.round(Math.random())) ? 'top' : 'bottom'

	this.ball = new Symbol(new Path.Circle({
		point: [dim, dim],
		radius: dim * 2,
		fillColor: this.getColour()
	})).place(this.origin)

    this.ball.position.x = Math.round(Math.random() * window.innerWidth)
    this.ball.position.y = Math.round(Math.random() * window.innerHeight)

	this.velocity = {
        x: (Math.random() < .5 ? -1 : 1) * Math.random() * .7,
        y: (Math.random() < .5 ? -1 : 1) * Math.random() * .7
	}
	
	this.ball.scale(Math.random())
	// this.ball.opacity = Math.random()

	this.ball.on('frame', this.onFrame.bind(this))
	window.addEventListener('resize', this.handleResize.bind(this))
	element.addEventListener('click', this.handleClick.bind(this))
	//element.addEventListener('mousemove', this.handleMouseOver.bind(this))
	//element.addEventListener('mouseleave', this.handleMouseLeave.bind(this))

	return this.ball
}

Ball.prototype = {
	getColour: function() {
		return colours[Math.floor(Math.random() * colours.length)]
	},

	onFrame: function(e) {
		if (this.hasMouse)
			return this.gravitate()

		else if (this.hasScatter)
			return this.scatter()

		else return this.oscillate()
	},

	oscillate: function() {
		this.ball.position.x += this.velocity.x
		this.ball.position.y += this.velocity.y
	},

	gravitate: function() {
		this.ball.position.x += (this.mouse.x - this.ball.position.x) / this.gravitate_speed
		this.ball.position.y += (this.mouse.y - this.ball.position.y) / this.gravitate_speed
	},

	scatter: function() {
		this.ball.position.x += (this.firstX - this.ball.position.x) / this.scatter_speed
		this.ball.position.y += (this.firstY - this.ball.position.y) / this.scatter_speed

		if (parseInt(this.ball.position.x) === parseInt(this.firstX))
			this.hasScatter = false
	},

	handleMouseOver: function(e) {
		this.mouse = { x: e.pageX, y: e.pageY }
		this.hasMouse = !this.hasScatter
	},

	handleMouseLeave: function(e) { 
		this.hasMouse = false 
		this.handleClick()
	},

	handleClick: function(e) { 
		e.stopPropagation()

	    this.firstX = Math.round(Math.random() * window.innerWidth)
   		this.firstY = Math.round(Math.random() * window.innerHeight)

		this.hasScatter = true 
	},

	handleResize: function() {
		this.ball.radius = dim * 2
	}
}

module.exports = Ball