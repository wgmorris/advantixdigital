import React from 'react'
import Ball from './Ball'

// ---------------------------------------------

class PaperDots extends React.Component {
	constructor(props) {
		super(props)
	}

	componentWillMount() { 
		paper.install(window) 
	}
	
	componentDidMount() {
		this.$el = document.getElementById('dotsCanvas')
		if (!this.$el) return

		paper.setup(this.$el)

		for (let i = 0; i < this.props.count; i++) {
			let center = Point.random().multiply(view.size),
				ball = new Ball(center, this.$el)
		}
		
		setTimeout(() => {
			this.$el.classList.add('loaded')
		}, 200)
	}

	componentWillUnmount() {
		if (!this.$el) return
		this.$el.classList.remove('loaded')
		paper.remove() 
	}

	render() {
		if (window.innerWidth < 786) 
			return null
		
		return <canvas id="dotsCanvas" className="full-sq" data-paper-resize={true} data-paperkeepalive={true} />
	}
}

module.exports = PaperDots