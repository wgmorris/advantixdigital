import _ from 'lodash'

// ---------------------------------------------

export default {
	Pages: [],
	blogID: 721,
	insightID: 99,
	teamID: 73,
	servicesID: 78,

	Get: function(filter) {
		return _.filter(this.Pages, filter)
	},

	Some: function(filter, length) {
		return this.Get(filter).splice(0, length)
	},

	Within: function(ids, length) {
		ids = _.map(ids, _.ary(parseInt, 1))
		let match = this.Get(o => {
			return _.indexOf(ids, o.id) > -1
		})

		if (length > 0) match.splice(0, length)

		return match
	},

	getUrl: function(id) {
		return _.first(this.Get({
			id: id
		})).url
	},

	updateMeta: function(id, meta, value) {
		let idx = _.findIndex(this.Pages, {
			id: id
		})
		
		this.Pages[idx].fields[meta] = value
	}
}