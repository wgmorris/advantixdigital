import hoverintent from 'hoverintent'

module.exports = {
	bind: function($links) {
		for (let i = 0; i < $links.length; i++) {
			hoverintent($links[i], function() {
				this.className = 'has-children over'
			}, function() {
				this.className = 'has-children'
			}).options({
				timeout: 200,
				interval: 50
			})
		}
	},

	destroy: function($links) {
		for (let i = 0; i < $links.length; i++)
			hoverintent.remove($links[i])
	}
}