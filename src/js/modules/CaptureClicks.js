import React from 'react'
import { browserHistory } from 'react-router'
import _ from 'lodash'

// ---------------------------------------------

class CaptureClicks extends React.Component {
	constructor(props) {
		super(props)
	}

	componentDidMount() {
		let $anchors = this.$zone.querySelectorAll('a:not([data-reactid])')

		for (let i = 0; i < $anchors.length; i++)
			$anchors[i].addEventListener('click', (e) => {
				let href = $anchors[i].getAttribute('href')

				if (_.indexOf(location.origin, href) > -1 && !$anchors[i].getAttribute('target')) {
					e.preventDefault()

					let slug = href.replace(/.*\/\/[^\/]*/, '')
					browserHistory.push(slug)
				}
			})
	}

	render() {
		return <div id="container" ref={(c) => this.$zone = c}>{this.props.children}</div>
	}
}

module.exports = CaptureClicks