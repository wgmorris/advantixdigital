import _ from 'lodash'
import reqwest from 'reqwest'
import PageStore from '../stores/PageStore'
import env from '../constants/env'

// ---------------------------------------------

module.exports = {
	pathname: null, search: {},
	errpage: {
		title: '404: Page not found',
		content: '<p>The page you were looking for couldn\'t be found. Please try again by using the navigation above.</p>',
		is_blog: false, is_home: false,
		fields: { mast_copy: '' },
		seo: {
			title: '404: Page not found',
			desc: '',
			keywords: ''
		}
	},

	buildSearch: function() {
		this.search = { is_home: true }

		if (this.pathname && this.pathname != '/')
			this.search = { url: this.pathname }

		if (/category/.test(this.pathname))
			this.search = { is_blog: true }
	},

	update: function(pathname, callback) {
		this.pathname = pathname
		this.buildSearch()

		let tmp = null

		if (
			!_.isEmpty(PageStore.Pages)
			&& (tmp = PageStore.Get(this.search)[0])
		) callback(tmp)

		else {
			let xhr = reqwest({
				url: env + 'data.js?only=spost&p=' + pathname,
				method: 'GET',
				dataType: 'application/json'
			})

			xhr.then(response => {
				if (!_.isEmpty(response.pages))
					callback(response.pages[0])

				else callback(this.errpage)
			})

			xhr.fail(error => {
				console.error('Page error:', error)
				callback(this.errpage)
			})
		}
	}
}