require('classlist-polyfill')

import React from 'react'
import { render } from 'react-dom'
import { Router, Route, IndexRoute, browserHistory } from 'react-router'

import Layout from './components/Layout'
import HomeTemplate from './components/Template-Home'
import TemplateHandler from './components/TemplateHandler'

// ---------------------------------------------

render((
<Router onUpdate={() => {
	if (!/(blog|insights)/.test(location.pathname))
		window.scrollTo(0, 0)
}} history={browserHistory}>
<Route path="/" component={Layout}>
	<IndexRoute component={HomeTemplate} />

	<Route path=":parent" component={TemplateHandler}>
		<Route path=":child" />
	</Route>
</Route>
</Router>
), document.getElementById('app'))