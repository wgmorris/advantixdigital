import React from 'react'

// ---------------------------------------------

class Lead extends React.Component {
	constructor(props) {
		super(props)
	}

	render() {
		if (!this.props.lead)
			return null
		
		return <div className="lead" dangerouslySetInnerHTML={{__html: this.props.lead || ''}} />
	}
}

module.exports = Lead