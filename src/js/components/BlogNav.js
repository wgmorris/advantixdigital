import React from 'react'
import { Link } from 'react-router'
import _ from 'lodash'
import PageStore from '../stores/PageStore'

// ---------------------------------------------

class BlogNav extends React.Component {
	constructor(props) {
		super(props)
	}

	render() {
		if (_.isEmpty(PageStore.Get({
			type: 'post',
			category: ['Industry Guides']
		})))
			return null

		return (
		<nav className="blog-nav center-align">
		<div className="row wrapper">
			<ul>
				<li>
					<Link to={PageStore.getUrl(PageStore.blogID)} activeClassName="active">
						<i className="icon-ptype-blog" />
						Blog Posts
					</Link>
				</li>

				<li>
					<Link to={PageStore.getUrl(PageStore.insightID)} activeClassName="active">
						<i className="icon-ptype-guides" />
						Industry Guides
					</Link>
				</li>
			</ul>
		</div>
		</nav>
		)
	}
}

module.exports = BlogNav