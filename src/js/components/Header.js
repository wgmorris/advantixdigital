import React from 'react'
import { IndexLink, Link } from 'react-router'
import _ from 'lodash'
import Menu from './Menu'
import MobileMenu from './MobileMenu'
import HoverIntent from '../modules/HoverIntent'

// ---------------------------------------------

class Header extends React.Component {
	constructor(props) {
		super(props)
		this.lastScrollTop = 0
		this._onScroll = this.handleScroll.bind(this)
	}

	componentDidMount() {
		window.addEventListener('scroll', this._onScroll, false)
		HoverIntent.bind(this.$header.getElementsByClassName('has-children'))
	}

	componentWillUnmount() {
		window.removeEventListener('scroll', this._onScroll, false)
	}

	componentDidUpdate() {
		this.$header.classList.remove('mobile-menu-open')
	}

	toggleIf(c, cond) {
		let classList = this.$header.classList

		if (cond) classList.add(c)
		else classList.remove(c)
	}

	handleScroll(e) {
		let scrollTop = window.pageYOffset
		let threshold = 162

		// this.toggleIf('out', scrollTop >= threshold && this.lastScrollTop < scrollTop)
		this.toggleIf('min', scrollTop >= threshold)

		this.lastScrollTop = scrollTop
	}

	handleClick(e) {
		this.toggleIf(
			'mobile-menu-open',
			!this.$header.classList.contains('mobile-menu-open')
		)
	}

	render() {
		return (
		<header id="header" ref={(c) => this.$header = c}>
			<div className="row">
				<div className="s12 m3 col">
					<IndexLink className="logo" to="/">
						<img src="/img/logo.png" />
						<img src="/img/logo-small.png" />
					</IndexLink>
				</div>

				<div className="s12 m9 col right-align">
					<div className="hide-on-small-only">
						<Menu menu={this.props.header} />
					</div>

					<div className="hide-on-med-and-up mobile-link">
						<a href="javascript:;" onClick={this.handleClick.bind(this)}>
							<span></span>
						</a>
					</div>
				</div>
			</div>

			<MobileMenu menu={this.props.mobile} />
		</header>
		)
	}
}

module.exports = Header