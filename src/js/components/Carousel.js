import React from 'react'
import { Link } from 'react-router'
import { lory } from 'lory.js'
import _ from 'lodash'
import If from '../modules/If'
import PageStore from '../stores/PageStore'

class Carousel extends React.Component {
	constructor(props) {
		super(props)
	}

	componentDidUpdate() {
        lory(document.querySelector('.js_slider'));
	}

	render() {
		return (
		<div className="image-carousel js_slider">
			<div className="frame js_frame">
				<ul className="js_slides">
				{this.props.fields.slide.map(slide => {
					return <Slide key={Math.random()} {...slide} />
				})}
				</ul>
			</div>

			<span className="prev js_prev">
				<svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 501.5 501.5"><g><path fill="#2E435A" d="M302.67 90.877l55.77 55.508L254.575 250.75 358.44 355.116l-55.77 55.506L143.56 250.75z"/></g></svg>
			</span>
			<span className="next js_next">
				<svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 501.5 501.5"><g><path fill="#2E435A" d="M199.33 410.622l-55.77-55.508L247.425 250.75 143.56 146.384l55.77-55.507L358.44 250.75z"/></g></svg>
			</span>
		</div>
		)
	}
}

class Slide extends React.Component {
	constructor(props) {
		super(props)
	}

	render() {
		return (
		<li className="js_slide">
			<img src={this.props.image} />
		</li>
		)
	}
}

module.exports = Carousel