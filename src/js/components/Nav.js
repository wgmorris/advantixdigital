import React from 'react'
import _ from 'lodash'
import { IndexLink, Link } from 'react-router'
import classNames from 'classnames'
import env from '../constants/env'

// ---------------------------------------------

class Nav extends React.Component {
	constructor(props) {
		super(props)
	}

	generateListItem(a, callback) {
		let slug = a.url,
			_link = null

		if (slug === env) {
			_link = <IndexLink activeClassName="active" to="/">Home</IndexLink>
		}

		else if (
			slug.indexOf('advantix') > -1
			|| slug.indexOf('http') === -1
			&& slug.indexOf('mailto') === -1
			&& slug.indexOf('tel') === -1
			&& slug.indexOf('skype') === -1
		) {
			slug = a.url.replace(/.*\/\/[^\/]*/, '')

			_link = <Link to={slug} activeClassName="active" dangerouslySetInnerHTML={{__html: a.title}}></Link>
		}

		else {
			_link = <a href={slug} rel="external" target="_blank" dangerouslySetInnerHTML={{__html: a.title}}></a>
		}

		return (
		<li key={slug} className={classNames({
			'has-children': true
		})}>
			{_link} {callback ? callback.call() : null}
		</li>
		)
	}

	render() {
		return (
		<ul>
		{this.props.links.map(a => {
			return this.generateListItem(a, () => {
				return (
				<ul className="sub-menu">
				{(a.children || []).map(c => {
					return this.generateListItem(c)
				})}
				</ul>
				)
			})
		})}
		</ul>
		)
	}
}

module.exports = Nav