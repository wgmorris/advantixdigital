import React from 'react'
import _ from 'lodash'
import If from '../modules/If'
import Masthead from './Masthead'
import QuoteBlock from './QuoteBlock'

// ---------------------------------------------

class ClientsTemplate extends React.Component {
	constructor(props) {
		super(props)
	}

	render() {
		return (
		<div className="template-page template-clients">
			<Masthead {...this.props.page} />
			<RecentCaseStudies {...this.props.page.fields} />
			<ClientHistory {...this.props.page.fields} />
		</div>
		)
	}
}

// ---------------------------------------------

class RecentCaseStudies extends React.Component {
	constructor(props) {
		super(props)

		this._mouseover = this.handleMouseover.bind(this)
		this._mouseleave = this.handleMouseleave.bind(this)
	}

	componentDidMount() {
		this.$circles = document.getElementsByClassName('circle')

		if (window.innerWidth < 786) return
		for (let i = 0; i < this.$circles.length; i++) {
			this.setCircleData(this.$circles[i])
			this.$circles[i].addEventListener('mouseenter', this._mouseover)
			this.$circles[i].addEventListener('mouseleave', this._mouseleave)
		}

		this.handleMouseleave()
	}

	componentWillUnmount() {
		if (window.innerWidth < 786) return
			
		for (let i = 0; i < this.$circles.length; i++) {
			this.$circles[i].removeEventListener('mouseenter', this._mouseover)
			this.$circles[i].removeEventListener('mouseleave', this._mouseleave)
		}
	}

	setCircleData($circle) {
		let _d = (k, v) => $circle.setAttribute('data-' + k, v)

		_d('top', $circle.offsetTop)
		_d('left', $circle.offsetLeft)
		_d('radius', $circle.clientWidth / 2)
	}

	getCircleData($circle) {
		return {
			top: parseInt($circle.getAttribute('data-top')),
			left: parseInt($circle.getAttribute('data-left')),
			radius: parseInt($circle.getAttribute('data-radius'))
		}
	}

	handleMouseover(e) {
		let $circle = e.target,
			expand = 216,
			circleData = this.getCircleData($circle),
			hoveredX = circleData.left + circleData.radius + (expand / 2),
			hoveredY = circleData.top + circleData.radius + (expand / 2),
			newRadius = (2 * circleData.radius) + expand
		
		$circle.style.width = newRadius + 'px'
		$circle.style.height = newRadius + 'px'
		$circle.style.top = (circleData.top - (expand / 2)) + 'px'
		$circle.style.left = (circleData.left - (expand / 2)) + 'px'
	
		for (let i = 0; i < this.$circles.length; i++) {
			let $sibling = this.$circles[i]

			if ($sibling !== $circle) {
				let siblingData = this.getCircleData($sibling),
					siblingX = siblingData.left + siblingData.radius,
					siblingY = siblingData.top + siblingData.radius,
					angle = Math.atan2(hoveredY - siblingY, hoveredX - siblingX),
					distTop = (expand / 2) + Math.sin(angle) - 30,
					distLeft = (expand / 2) + Math.cos(angle) - 30

				if (siblingData.left >= circleData.left)
					$sibling.style.left = (siblingData.left + distLeft) + 'px'

				else $sibling.style.left = (siblingData.left - distLeft) + 'px'

				if (siblingData.top >= circleData.top)
					$sibling.style.top = (siblingData.top + distTop) + 'px'

				else $sibling.style.top = (siblingData.top - distTop) + 'px'
			}
		}
	}

	handleMouseleave() {
		for (let i = 0; i < this.$circles.length; i++) {
			let $circle = this.$circles[i],
				data = this.getCircleData($circle)

			$circle.style.width = (2 * data.radius) + 'px'
			$circle.style.height = (2 * data.radius) + 'px'
			$circle.style.top = data.top + 'px'
			$circle.style.left = data.left + 'px'
		}
	}

	render() {
		if (_.isEmpty(this.props.recent_case_studies))
			return null

		return (
		<div className="page-section offwhite">
		<div className="row wrapper center-align">
			<h2>Success Stories</h2>
			
			<div className="testimonials">
			{_.toArray(this.props.recent_case_studies).map(caseStudy => {
				return (
				<figure key={Math.random()} className="circle" style={{
					backgroundImage: 'url(' + (caseStudy.bg_image || '/img/filler-client.jpg') + ')'
				}}>
					<figcaption className="cover">
					<div className="centerY full-width">
						<div dangerouslySetInnerHTML={{__html: caseStudy.copy}} />

						<If test={caseStudy.title}>
							<cite dangerouslySetInnerHTML={{__html: '&ndash; ' + caseStudy.title}} />
						</If>
					</div>
					</figcaption>
				</figure>
				)
			})}
			</div>
		</div>
		</div>
		)
	}
}

// ---------------------------------------------

class ClientHistory extends React.Component {
	constructor(props) {
		super(props)
		this.state = { rows: 4 }

		this._handleResize = this.handleResize.bind(this)
	}

	componentDidMount() {
		setTimeout(this._handleResize, 100)
		window.addEventListener('resize', this._handleResize)
		window.addEventListener('orientationchange', this._handleResize)
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this._handleResize)
		window.removeEventListener('orientationchange', this._handleResize)
	}

	handleResize(e) {
		let rows = 4

		if (window.innerWidth < 786)
			rows = 2

		this.setState({ rows: rows })
	}

	render() {
		if (_.isEmpty(this.props.clients))
			return null

		return (
		<div className="client-history page-section">
		<div className="row wrapper center-align">
			<h2>History of Success</h2>
			<hr />

			<div className="clients table full-width">
			{_.chunk(this.props.clients, this.state.rows).map(clientRow => {
				return (
				<div key={Math.random()} className="row">
				{clientRow.map(client => {
					return (
					<figure key={Math.random()} className="cell">
						<img src={client.logo} />
						
						<figcaption style={{backgroundColor: client.bg_colour}}>
							<p dangerouslySetInnerHTML={{__html: client.name}} />
						</figcaption>
					</figure>
					)
				})}
				</div>
				)
			})}
			</div>
		</div>
		</div>
		)
	}
}

// ---------------------------------------------


module.exports = ClientsTemplate