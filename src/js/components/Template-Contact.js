import React from 'react'
import _ from 'lodash'
import If from '../modules/If'
import Masthead from './Masthead'
import GravityForm from './GravityForm'

// ---------------------------------------------

class ContactTemplate extends React.Component {
	constructor(props) {
		super(props)
		this.state = {}
	}

	componentDidMount() {
		document.getElementById('footer').classList.add('no-slide')

		this.setState({
			phone: this.props.page.fields.phone || false,
			email: this.props.page.fields.email || false,
			location: this.props.page.fields.location || false
		})
	}

	componentWillUnmount() {
		document.getElementById('footer').classList.remove('no-slide')
	}

	render() {
		if (_.isEmpty(this.props.page.fields))
			return null

		return (
		<div className="template-page template-contact">
			<Masthead {...this.props.page} />

			<div className="page-section center-align">
			<div className="row wrapper">
				<div dangerouslySetInnerHTML={{__html: this.props.page.fields.top_copy}} />

				<div className="contact-bar icons-bar">
					<If test={this.state.phone}>
						<a href={'tel:' + this.state.phone} target="_blank">
							<i className="icon-phone" />
							<span dangerouslySetInnerHTML={{__html: this.state.phone}} />
						</a>
					</If>

					<If test={this.state.email}>
						<a href={'mailto:' + this.state.email} target="_blank">
							<i className="icon-email" />
							<span dangerouslySetInnerHTML={{__html: this.state.email}} />
						</a>
					</If>

					<If test={this.state.location}>
						<a href={this.props.page.fields.location_url} target="_blank">
							<i className="icon-location" />
							<span dangerouslySetInnerHTML={{__html: this.state.location}} />
						</a>
					</If>
				</div>

				<GravityForm form={this.props.data.forms[0]} />
			</div>
			</div>
		</div>
		)
	}
}

module.exports = ContactTemplate