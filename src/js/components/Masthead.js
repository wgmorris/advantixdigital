import React from 'react'
import detectIe from 'detectIe'
import _ from 'lodash'
import If from '../modules/If'
import PostsPagination from './PostsPagination'
import PaperDots from '../paper/PaperDots'

// ---------------------------------------------

class Masthead extends React.Component {
	constructor(props) {
		super(props)
		this._handleScroll = this.handleScroll.bind(this)
	}

	componentDidMount() {
		if (!this.$masthead)
			return

		let bg = this.props.fields.mast_bg || false

		if (bg) {
			let img = new Image()
			img.src = bg
			img.onload = () => {
				if (!this.$masthead) return
				this.$masthead.style.backgroundImage = 'url(' + bg + ')'
			}
		}

		else this.$masthead.style.backgroundImage = 'url()'

		window.addEventListener('scroll', this._handleScroll)
	}

	componentWillUnmount() {
		window.removeEventListener('scroll', this._handleScroll)
	}

	scrollDown(e) {
		Velocity(document.body, 'stop')
		Velocity(document.body, 'scroll', {
			duration: 700,
			offset: window.innerHeight
		})
	}

	handleScroll(e) {
		this.$anchor.style.opacity = 1 - (window.scrollY / (window.innerHeight / 2.25) || 0)
	}

	render() {
		return (
		<div id="masthead" className="full-page" ref={(c) => this.$masthead = c}>
			<If test={!detectIe() && this.props.type !== 'team_member'}>
				<PaperDots count={50} />
			</If>

			<div className="centerY">
			<div className="wrapper">
				<h1 dangerouslySetInnerHTML={{__html: this.props.fields.mast_title || this.props.title}} />
				<hr />

				<div dangerouslySetInnerHTML={{__html: this.props.fields.mast_copy || ''}} />
				<PostsPagination {...this.props} />
			</div>
			</div>

			<a
				ref={(c) => this.$anchor = c}
				className="scroll-down-anchor"
				onClick={this.scrollDown.bind(this)} />
		</div>
		)
	}
}

// ---------------------------------------------

module.exports = Masthead