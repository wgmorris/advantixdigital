import React from 'react'
import _ from 'lodash'
import Menu from './Menu'
import TrySomethingNew from './PageSlide-TrySomethingNew'

// ---------------------------------------------

class Footer extends React.Component {
	constructor(props) {
		super(props)
		this._onResize = this.handleResize.bind(this)
		this._handleScroll = this.handleScroll.bind(this)
	}

	componentDidMount() {
		setTimeout(this._onResize, 100)
		setTimeout(this._handleScroll, 100)

		window.addEventListener('resize', this._onResize)
		window.addEventListener('scroll', this._handleScroll)
	}

	componentWillUnmount() {
		window.removeEventListener('scroll', this._handleScroll)
		window.removeEventListener('resize', this._onResize)
	}

	componentDidUpdate() {
		setTimeout(this._onResize, 100)
	}

	handleResize() {
		this.$push.style.height = (this.$footer.scrollHeight - 20) + 'px'
	}

	handleClick(e) {
		e.preventDefault()

		Velocity(document.body, 'stop')
		Velocity(document.body, 'scroll', {
			offset: 0,
			duration: 650
		})
	}

	handleScroll(e) {
		let offset = window.innerHeight / 2.25

		if (window.innerWidth < 786) {
			if (window.scrollY >= this.$footer.offsetTop - 350)
				this.$anchor.style.opacity = 1

			else this.$anchor.style.opacity = 0
		}

		else this.$anchor.style.opacity = window.scrollY / offset || 0
	}

	render() {
		if (_.isEmpty(this.props))
			return null

		return (<span>
		<footer id="footer" className="row" ref={(c) => this.$footer = c}>
			<TrySomethingNew resizeHandler={this._onResize} {...this.props} />

			<div id="footer-bottom">
				<div className="s12 m6 col clearfix">
					<Menu menu={this.props.data.menus.footer} />

					<p>
						&copy;{(new Date().getFullYear())} Advantix Digital.
					</p>

					<div className="social-links">
						<a href="//www.facebook.com/advantixdigital" target="_blank">
							<i className="icon-facebook" />
						</a>

						<a href="//twitter.com/advantixdigital" target="_blank">
							<i className="icon-twitter" />
						</a>

						<a href="//www.linkedin.com/company/advantix-digital" target="_blank">
							<i className="icon-linkedin" />
						</a>

						<a href="//www.instagram.com/advantixdigital/" target="_blank">
							<i className="icon-instagram" />
						</a>

						<a href="//plus.google.com/+AdvantixMarketing/" target="_blank">
							<i className="icon-googleplus" />
						</a>
					</div>
				</div>

				<div className="s12 m6 col right-align">
					<a href="javascript:;"
						ref={(c) => this.$anchor = c}
						className="to-top"
						onClick={this.handleClick.bind(this)} />

					<p>
						Call Us: <a href={'tel:' + this.props.data.site.theme_options.phone} dangerouslySetInnerHTML={{__html: this.props.data.site.theme_options.phone}} />
						<br />
						<a href="//goo.gl/maps/kyHXgAb8zkx" target="_blank">14285 Midway Rd #475 Addison, TX 75001</a>
					</p>
				</div>
			</div>
		</footer>

		<div id="footer-push" ref={(c) => this.$push = c} />
		</span>
		)
	}
}

module.exports = Footer