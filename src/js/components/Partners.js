import React from 'react'
import _ from 'lodash'

// ---------------------------------------------

export default class Partners extends React.Component {
	constructor(props) {
		super(props)
		this.state = { rows : 6 }

		this._handleResize = this.handleResize.bind(this)
	}

	componentDidMount() {
		setTimeout(this._handleResize, 100)
		window.addEventListener('resize', this._handleResize)
		window.addEventListener('orientationchange', this._handleResize)
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this._handleResize)
		window.removeEventListener('orientationchange', this._handleResize)
	}

	handleResize(e) {
		let rows = 6

		if (window.innerWidth < 786)
			rows = 2

		else if (window.innerWidth < 1200)
			rows = 3

		this.setState({ rows: rows })
	}

	render() {
		if (_.isEmpty(this.props.partners))
			return null

		return (
		<div className="partners page-section center-align">
			<h3 dangerouslySetInnerHTML={{__html: this.props.partners_title}} />

			<div className="partners table full-width">
			{_.chunk(this.props.partners, this.state.rows).map(partnersRow => {
				return (
				<div key={Math.random()} className="row">
				{partnersRow.map(partner => {
					return (
					<figure key={Math.random()} className="cell">
						<a href={partner.url || 'javascript:;'} title="_blank">
							<img alt={partner.title} src={partner.logo} />
						</a>
					</figure>
					)
				})}
				</div>
				)
			})}
			</div>
		</div>
		)
	}
}