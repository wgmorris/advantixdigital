import React from 'react'
import { Link } from 'react-router'
import _ from 'lodash'
import If from '../modules/If'

// ---------------------------------------------

class MissionPageSlide extends React.Component {
	constructor(props) {
		super(props)
	}

	render() {
		if (_.isEmpty(this.props) || !this.props.mission_copy)
			return null

		return (
		<div className="full-page slide-mission darken-bg" data-title="Our Mission">
			<div className="centerY">
				<div className="row wrapper">
					<div className="mission-deco s12 m6 col">
						<h3>Our Mission</h3>
					</div>

					<div className="mission-text s12 m6 col">
						<div dangerouslySetInnerHTML={{__html: this.props.mission_copy}} />

						<If test={this.props.mission_cta_url && this.props.mission_cta_text}>
							<Link to={this.props.mission_cta_url} className="btn">
								{this.props.mission_cta_text}
								<i className="material-icons">trending_flat</i>
							</Link>
						</If>
					</div>
				</div>
			</div>
		</div>
		)
	}
}

module.exports = MissionPageSlide