import React from 'react'
import { browserHistory, Link } from 'react-router'
import _ from 'lodash'
import Masthead from './Masthead'
import PageStore from '../stores/PageStore'

// ---------------------------------------------

class TeamTemplate extends React.Component {
	constructor(props) {
		super(props)
	}

	render() {
		return (
		<div className="template-page template-team">
			<Masthead {...this.props.page} />

			<div className="page-section center-align">
			<div className="row wrapper page-contents">
				<div className="row team-list">
				{PageStore.Get({
					type: 'team_member'
				}).map(teamMember => {
					return <TeamMember key={Math.random()} {...teamMember} />
				})}
				</div>
			</div>
			</div>
		</div>
		)
	}
}

// ---------------------------------------------

class TeamMember extends React.Component {
	constructor(props) {
		super(props)
	}

	componentDidMount() {
		document.body.querySelector('#header a[href="/about/"]').classList.remove('active')
		document.getElementById('footer').classList.add('no-slide')
	}

	componentWillUnmount() {
		document.getElementById('footer').classList.remove('no-slide')
	}

	handleClick(e) {
		browserHistory.push(this.props.url)
	}

	render() {
		return (
		<figure className="team-member s12 m4 l1 col" onClick={this.handleClick.bind(this)}>
			<img src={this.props.fields.photo_1 || '/img/no-photo.jpg'} className="full-width" />
			<img src={this.props.fields.photo_2 || '/img/no-photo.jpg'} className="full-width invisible cover" />

			<figcaption>
			<div className="reveal-content">
				{this.props.title}

				<small dangerouslySetInnerHTML={{__html: this.props.fields.position}} />

				<Link to={this.props.url}>
					See Full Bio
					<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAHCAYAAADXhRcnAAAAbklEQVQYV5XOsQmEYBCE0W8So6vCSMxs4rAU7UI4OFNbsA87sAZLEOxg5Ac3XzfeNzMiebZ74JS0B1HSYnsAZuAbAWlcSmyPwD8CZPvItj9/FfABmoLrF7gFVuAnaUnPtt0BW8BS+AZPwFUaY+kNgd4iHJlDsjwAAAAASUVORK5CYII=" />
				</Link>
			</div>
			</figcaption>
		</figure>
		)
	}
}

// ---------------------------------------------

module.exports = TeamTemplate