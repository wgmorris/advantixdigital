import React from 'react'
import Menu from './Menu'

// ---------------------------------------------

class MobileMenu extends React.Component {
	constructor(props) {
		super(props)
	}

	render() {
		return (
		<div id="mobile-menu">
			<Menu {...this.props} />
		</div>
		)
	}
}

module.exports = MobileMenu