import React from 'react'
import _ from 'lodash'
import raf from 'raf'
import wheel from 'wheel'
import detectIe from 'detectIe'
import Transform from './PageSlide-Transform'
import Mission from './PageSlide-Mission'
import Skills from './PageSlide-Skilled'
import Testimonials from './PageSlide-Testimonials'
import Insight from './PageSlide-Insight'

// ---------------------------------------------

class HomeTemplate extends React.Component {
	constructor(props) {
		super(props)

		this.state = {}
		this.curIdx = 0

		this.isAnimating = false
		this.scrollTimeout = null
		this._handleScroll = this.handleScroll.bind(this)
		this._handleKeyDown = this.handleKeyDown.bind(this)
	}

	componentDidMount() {
		document.body.classList.add('home')

		if (window.innerWidth > 786) {
			wheel.addWheelListener(document.body, this._handleScroll, true)
			document.body.addEventListener('keydown', this._handleKeyDown)
		}

		let $sections = document.getElementsByClassName('full-page')

		this.setState({
			$sections: $sections,
			max: $sections.length - 1
		})

		setTimeout(() => {
			this.goToSection(0)
		}, 100)
	}

	componentWillUnmount() {
		document.body.classList.remove('home')

		if (window.innerWidth > 786) {
			wheel.removeWheelListener(document.body, this._handleScroll, true)
			document.body.removeEventListener('keydown', this._handleKeyDown)
		}
	}

	handleScroll(e) {
		if (!document.body.classList.contains('home'))
			return true

		e.preventDefault()
		clearTimeout(this.scrollTimeout)

		if (this.isAnimating) return

		this.scrollTimeout = setTimeout(() => {
			let direction = (
				e.detail < 0 || e.wheelDelta > 0
				|| (e.originalEvent && e.originalEvent.wheelDelta > 0)
			) ? 1 : -1

			if (direction === -1)
				this.goToSection(this.curIdx + 1)

			else this.goToSection(this.curIdx - 1)
		}, 250)
	}

	handleKeyDown(e) {
		let k = e.keyCode

		if (k >= 37 && k <= 40) {
			e.preventDefault()

			if (k <= 38)
				this.goToSection(this.curIdx - 1)

			else this.goToSection(this.curIdx + 1)
		}
	}

	handleClick(e) {
		this.goToSection(this.curIdx + 1)
	}

	goToSection(idx) {
		if (_.isEmpty(this.state))
			return

		if (idx < 0) idx = 0
		else if (idx > this.state.max) idx = this.state.max

		this.state.$sections[this.curIdx].classList.remove('active')
		if (this.curIdx < idx) this.state.$sections[this.curIdx].classList.add('out')

		this.state.$sections[idx].classList.remove('out')
		this.state.$sections[idx].classList.add('active')

		for (let i = 0; i < this.state.max + 1; i++)
			document.querySelectorAll('#page-slide-nav a')[i].classList.remove('active')

		document.querySelectorAll('#page-slide-nav a')[idx].classList.add('active')

		Velocity(document.body, 'stop')
		Velocity(document.body, 'scroll', {
			duration: 750,
			offset: (idx * window.innerHeight),
			complete: (els) => {
				// location.hash = '!/' + this.state.$sections[this.curIdx].getAttribute('data-title').toLowerCase()
			}
		})

		document.body.classList.remove('s-' + this.curIdx)
		this.curIdx = idx
		document.body.classList.add('s-' + this.curIdx)
	}

	render() {
		return (
		<div className="template-home">
			<Transform {...this.props.page.fields} clickHandler={this.handleClick.bind(this)} />
			<Mission {...this.props.page.fields} />
			<Skills {...this.props.page.fields} />
			<Testimonials {...this.props.page.fields} />
			<Insight {...this.props.page.fields} />
			<SlideNav {...this.state} animHandler={this.goToSection.bind(this)} />
		</div>
		)
	}
}

// ---------------------------------------------

class SlideNav extends React.Component {
	constructor(props) {
		super(props)
	}

	handleClick(idx, e) {
		this.props.animHandler(idx)
	}

	render() {
		if (!this.props.$sections)
			return null

		return (
		<nav id="page-slide-nav" className="hide-on-small-only">
		{[...Array(this.props.$sections.length)].map((x, i) => {
			let $s = this.props.$sections[i],
				className = i === 0 ? 'active' : 'inactive'

			return (
			<a
				key={Math.random()}
				href="javascript:;"
				onClick={this.handleClick.bind(this, i)}
				className={className}
			>
				<span>{$s.getAttribute('data-title')}</span>
			</a>
			)
		})}
		</nav>
		)
	}
}

// ---------------------------------------------

module.exports = HomeTemplate