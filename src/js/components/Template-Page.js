import React from 'react'
import _ from 'lodash'
import If from '../modules/If'
import Masthead from './Masthead'
import PageBlock from './PageBlock'
import QuoteBlock from './QuoteBlock'

// ---------------------------------------------

class PageTemplate extends React.Component {
	constructor(props) {
		super(props)
	}

	componentDidMount() {
		if (this.props.page.type === 'service')
			document.body.querySelector('#header ul li:nth-child(2) > a').classList.add('active')

		document.body.classList.add('page-id-' + this.props.page.id)
	}

	componentWillUnmount() {
		if (this.props.page.type === 'service')
			document.body.querySelector('#header ul li:nth-child(2) > a').classList.remove('active')

		document.body.classList.remove('page-id-' + this.props.page.id)
	}

	render() {
		if (_.isEmpty(this.props.page.fields))
			return null

		return (
		<div className="template-page">
			<Masthead {...this.props.page} />
			
			<If test={this.props.page.fields.content_sections}>
			{_.toArray(this.props.page.fields.content_sections).map(section => {
				switch (_.keys(section.type)[0]) {
					default:
						return <PageBlock key={Math.random()} {...section} data={this.props.data} />
					break

					case 'quote':
						return <QuoteBlock key={Math.random()} {...section} />
					break
				}
			})}
			</If>

			<If test={!this.props.page.fields.content_sections}>
				<PageBlock {...this.props.page} />
			</If>
		</div>
		)
	}
}

module.exports = PageTemplate