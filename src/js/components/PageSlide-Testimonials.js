import React from 'react'
import { Link } from 'react-router'
import _ from 'lodash'
import If from '../modules/If'

// ---------------------------------------------

class TestimonialsPageSlide extends React.Component {
	constructor(props) {
		super(props)
	}

	render() {
		if (_.isEmpty(this.props))
			return null

		return (
		<div className="full-page slide-testimonials" data-title="Results-Minded">
			<div className="centerY">
				<div className="row wrapper">
					<div dangerouslySetInnerHTML={{__html: this.props.testimonial_copy}} />
					<cite dangerouslySetInnerHTML={{__html: '- ' + this.props.testimonial_title}} />

					<If test={this.props.testimonial_cta_url && this.props.testimonial_cta_text}>
						<Link to={this.props.testimonial_cta_url} className="btn">
							{this.props.testimonial_cta_text}
							<i className="material-icons">trending_flat</i>
						</Link>
					</If>
				</div>

				<div className="row wrapper">
					<div className="table certs">
						<div className="cell">
							<strong>Certificates</strong>

							{this.props.certificates.map(img => {
								return <img key={Math.random()} alt={img.title} src={img.logo} />
							})}
						</div>
					</div>

					<div className="table certs as-seen-on">
						<div className="cell">
							<strong>As Seen On</strong>

							{this.props.as_seen_on.map(img => {
								return <img key={Math.random()} alt={img.title} src={img.logo} />
							})}
						</div>
					</div>
				</div>
			</div>
		</div>
		)
	}
}

module.exports = TestimonialsPageSlide