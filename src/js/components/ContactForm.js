import React from 'react'
import _ from 'lodash'

// ---------------------------------------------

module.exports = class ContactForm extends React.Component {
	constructor(props) {
		super(props)
	}

	render() {
		return (
		<form method="post" action="javascript:;">
			<div className="row">
				<div className="s12 m6 col">
					<input type="text" id="l_name" />
					<label for="l_name">Name</label>
				</div>

				<div className="s12 m6 col">
					<input type="text" id="l_company" />
					<label for="l_company">Company</label>
				</div>
			</div>

			<div className="row">
				<div className="s12 m6 col">
					<input type="text" id="l_email" />
					<label for="l_email">Email</label>
				</div>

				<div className="s12 m6 col">
					<input type="text" id="l_phone" />
					<label for="l_phone">Phone</label>
				</div>
			</div>

			<div className="row">
				<div className="s12 col">
					<textarea id="l_msg" />
					<label for="l_msg">Comment</label>
				</div>
			</div>

			<br />

			<button className="btn" type="submit">
				Send
				<i className="material-icons">trending_flat</i>
			</button>
		</form>
		)
	}
}