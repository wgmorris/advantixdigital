import React from 'react'
import { browserHistory, Link } from 'react-router'
import _ from 'lodash'
import PageStore from '../stores/PageStore'

// ---------------------------------------------

class TeamGrid extends React.Component {
	constructor(props) {
		super(props)
	}

	render() {
		if (_.isEmpty(this.props.photos))
			return null

		return (
		<div className="team-grid center-align">
			<figure className="promo w-2x1">
				<figcaption className="centerY full-width">
					<h2 dangerouslySetInnerHTML={{__html: this.props.promo_title}} />

					<div className="btn-group">
					{this.props.promo_links.map(l => {
						return <Link 
							key={Math.random()} 
							to={l.url} 
							className="btn btn-white" 
							dangerouslySetInnerHTML={{__html: l.title}} />
					})}
					</div>
				</figcaption>
			</figure>

			{this.props.photos.map(teamMember => {
				return <TeamMember key={Math.random()} {...teamMember} />
			})}
		</div>
		)
	}
}

// ---------------------------------------------

class TeamMember extends React.Component {
	constructor(props) {
		super(props)
	}

	render() {
		return <figure 
			alt={this.props.title} 
			className="w-1x1" 
			style={{ backgroundImage: 'url(' + this.props.image + ')'}} />
	}
}

// ---------------------------------------------

module.exports = TeamGrid