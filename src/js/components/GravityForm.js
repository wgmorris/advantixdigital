import React from 'react'
import { browserHistory } from 'react-router'
import _ from 'lodash'
import reqwest from 'reqwest'
import serialize from 'form-serialize'
import classNames from 'classnames'
import If from '../modules/If'
import PageStore from '../stores/PageStore'

// ---------------------------------------------

class GravityForm extends React.Component {
	constructor(props) {
		super(props)
		this.state = {}
	}

	isValid() {
		let errCounter = 0,
			$fields = this.$form.querySelectorAll('[required]'),
			$p = document.createElement('p')
	
		$p.className = 'err'
		$p.innerHTML = 'Field is required'

		for (let i = 0; i < $fields.length; i++) {
			let $field = $fields[i],
				alreadyHasError = $field.parentNode.querySelector('p')
			
			if ($field.value === '') {
				$field.parentNode.classList.add('invalid')

				if (!alreadyHasError)
					$field.parentNode.insertBefore($p, $field.nextElementSibling.nextElementSibling)

				errCounter++
			}

			else {
				$field.parentNode.classList.remove('invalid')

				if (alreadyHasError)
					$field.parentNode.querySelector('p').remove()
			}
		}

		return errCounter === 0
	}

	handleSubmit(e) {
		if (this.isValid()) {
			this.$form.classList.add('is-loading')

			let xhr = reqwest({
				url: 'https://advantixdigital.com/form.js',
				method: 'POST',
				type: 'json',
				data: serialize(this.$form, { hash: true }),
			})
			
			xhr.then(response => {
				this.setState({
					error: response.error,
					msg: response.msg.message,
				})

				if (this.props.onSuccess)
					this.props.onSuccess.call()

				if (this.props.form.confirmation.type === 'page') {
					browserHistory.push(PageStore.getUrl(
						this.props.form.confirmation.pageId
					))
				}
			})

			xhr.fail((error, response) => {
				this.setState({
					error: true,
					msg: 'Server encountered an issue. Please contact us and let us know.'
				})
			})

			xhr.always(response => {
				this.$form.classList.remove('is-loading')
			})
		}
	}

	render() {
		if (_.isEmpty(this.props.form))
			return null

		return (
		<form method="post" action="javascript:;" 
			ref={(c) => this.$form = c}
			className="browser-default left-align" 
			onSubmit={this.handleSubmit.bind(this)}
			noValidate
		>
			<If test={this.state.msg}>
				<div className={classNames('card-panel center-align', {
					'red darken-4': this.state.error,
					'green darken-2': !this.state.error
				})}>
					<p>{this.state.msg}</p>
				</div>
			</If>
			
			<If test={!this.state.msg}>
				{_.chunk(this.props.form.fields, 2).map(fields => {
					return (
					<div key={Math.random()} className="row">
					{fields.map(field => {
						return <GFInput key={Math.random()} {...field} />
					})}
					</div>
					)
				})}

				<input type="hidden" name="form_id" defaultValue={this.props.form.id} />
				<br />

				<div className="center-align">
					<button type="submit" className="btn">
						<span>
							{this.props.form.button.text || 'Submit'}
							<i className="material-icons">trending_flat</i>
						</span>

						<span>Submitting&hellip;</span>
					</button>
				</div>
			</If>
		</form>
		)
	}
}

// ---------------------------------------------

class GFInput extends React.Component {
	constructor(props) {
		super(props)
	}

	handleChange(e) {
		if (e.target.value !== '' && e.target.value !== e.target.defaultValue)
			e.target.classList.add('valid')

		else e.target.classList.remove('valid')
	}

	buildInput() {
		return <input 
			type={this.props.type}
			name={this.props.name} 
			required={this.props.isRequired}
			onBlur={this.handleChange.bind(this)} />
	}

	buildTextarea() {
		return <textarea 
			name={this.props.name} 
			required={this.props.isRequired}
			onBlur={this.handleChange.bind(this)} />
	}

	buildSelect() {
		return (
		<select 
			name={this.props.name} 
			required={this.props.isRequired}
			onBlur={this.handleChange.bind(this)}
		>
			<option defaultValue="">{this.props.title}</option>

			{this.props.choices.map(choice => {
				return (
				<option key={Math.random()} defaultValue={choice.value}>
					{choice.text}
				</option>
				)
			})}
		</select>
		)
	}

	render() {
		let fn = _.camelCase('build ' + this.props.type),
			field = _.isFunction(this[fn]) ? this[fn]() : this.buildInput()

		return (
		<div key={Math.random()} className="s12 m6 col">
			{field}

			<label>
				{this.props.title} <span className="astericks">*</span>
			</label>
		</div>
		)
	}
}

// ---------------------------------------------

module.exports = GravityForm