import React from 'react'
import _ from 'lodash'
import classNames from 'classnames'

// ---------------------------------------------

class QuoteBlock extends React.Component {
	constructor(props) {
		super(props)
	}

	render() {
		return (
		<div
			className={classNames('page-section bg-block full-bg', {
				hasLead: !_.isEmpty(this.props.lead)
			})}
			 
			style={{
				backgroundImage: 'url(' + this.props.bg_image || '/img/bg-pattern.png' + ')'
			}}
		>
		<div className="table row wrapper">
			<aside className="sidebar cell">
				<div dangerouslySetInnerHTML={{__html: this.props.lead || '<i class="icon-quote"></i>'}} />
			</aside>

			<div className="page-contents cell left-align">
				<blockquote>
					<div dangerouslySetInnerHTML={{__html: this.props.copy}} />
					<cite dangerouslySetInnerHTML={{__html: '&ndash; ' + this.props.title}} />
				</blockquote>
			</div>
		</div>
		</div>
		)
	}
}

module.exports = QuoteBlock