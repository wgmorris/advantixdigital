import React from 'react'
import _ from 'lodash'
import If from '../modules/If'
import PageStore from '../stores/PageStore'
import Masthead from './Masthead'
import BlogBar from './BlogBar'
import GravityForm from './GravityForm'

// ---------------------------------------------

class GatedTemplate extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			locked: true,
			content: this.props.page.excerpt
		}
	}

	componentWillMount() {
		this.key = 'unlocked-' + this.props.page.id

		if (Modernizr.localstorage && localStorage.getItem(this.key))
			this.handleSubmit()
	}

	componentDidMount() {
		document.body.querySelector('a[href="/blog/"]').classList.add('active')
		document.getElementById('footer').classList.add('no-slide')
	}

	componentWillUnmount() {
		document.body.querySelector('a[href="/blog/"]').classList.remove('active')
		document.getElementById('footer').classList.remove('no-slide')
	}

	handleSubmit() {
		this.setState({
			locked: false,
			content: this.props.page.content
		})

		PageStore.updateMeta(this.props.page.id, 'is_gated', false)

		if (Modernizr.localstorage)
			localStorage.setItem(this.key, 'true')
	}

	render() {
		if (_.isEmpty(this.props.page))
			return null

		return (
		<div className="template-page template-single template-gated">
			<Masthead {...this.props.page} />
			<BlogBar {...this.props.page} />

			<div className="page-section">
			<div className="row wrapper">
				<aside className="sidebar">
					<h2 dangerouslySetInnerHTML={{__html: this.props.page.title}} />
					<hr />
				</aside>

				<div className="page-contents" dangerouslySetInnerHTML={{__html: this.state.content}} />
			</div>
			</div>
		
			<If test={this.state.locked}>
				<div className="gated-form">
				<div className="row wrapper">
					<aside className="sidebar">&nbsp;</aside>

					<div className="page-contents">
						<h3>Please fill the form to continue reading&hellip;</h3>
						<GravityForm form={this.props.data.forms[1]} onSuccess={this.handleSubmit.bind(this)} />
					</div>
				</div>
				</div>
			</If>
		</div>
		)
	}
}

module.exports = GatedTemplate