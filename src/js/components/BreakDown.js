import React from 'react'
import _ from 'lodash'

// ---------------------------------------------

export default class BreakDown extends React.Component {
	constructor(props) {
		super(props)
	}

	render() {
		return (
		<div className="page-section texture-block center-align">
		<div className="row wrapper">
			<h2>Breaking Down Innovation</h2>
			<hr />

			<div className="breakdown">
				<div className="row">
					<figure>
						<figcaption className="centerY">
							<i className="icon-about-questions"></i>
							<strong>Ask Questions</strong>
						</figcaption>
					</figure>

					<hr />

					<figure>
						<figcaption className="centerY">
							<i className="icon-about-experiment"></i>
							<strong>Experiment</strong>
						</figcaption>
					</figure>

					<hr />

					<figure>
						<figcaption className="centerY">
							<i className="icon-about-experience"></i>
							<strong>Gain Exerpience</strong>
						</figcaption>
					</figure>
				</div>

				<label><span>Repeat</span></label>
			</div>
		</div>
		</div>
		)
	}
}