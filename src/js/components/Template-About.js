import React from 'react'
import _ from 'lodash'
import Masthead from './Masthead'
import TeamGrid from './TeamGrid'
import Partners from './Partners'

// ---------------------------------------------

class AboutTemplate extends React.Component {
	constructor(props) {
		super(props)
	}

	render() {
		return (
		<div className="template-page template-about">
			<Masthead {...this.props.page} mast_bg="/img/masthead-1.jpg" />

			<div className="page-section no-top-padding center-align">
				<TeamGrid {...this.props.page.fields} />

				<div className="journey">
				<div className="wrapper">
					<figure className="circle sm">
						<figcaption className="cover table">
						<div className="cell">
							<h3 dangerouslySetInnerHTML={{__html: this.props.page.fields.journey_title || ''}} />
						</div>
						</figcaption>
					</figure>

					{_.toArray(this.props.page.fields.circles).map(circle => {
						return (
						<figure key={Math.random()} className="circle">
							<figcaption className="cover table">
							<div className="cell">
								<h2
									style={{ color: circle.title_colour }}
									dangerouslySetInnerHTML={{__html: circle.title + '<small>' + circle.sub_title + '</small>'}} />

								<span dangerouslySetInnerHTML={{__html: circle.copy}} />
							</div>
							</figcaption>
						</figure>
						)
					})}
				</div>
				</div>
			</div>

			<Partners {...this.props.page.fields} />
		</div>
		)
	}
}

module.exports = AboutTemplate