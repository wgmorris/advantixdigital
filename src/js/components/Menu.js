import React from 'react'
import _ from 'lodash'
import Nav from '../components/Nav'

// ---------------------------------------------

class Menu extends React.Component {
	constructor(props) {
		super(props)
	}

	render() {
		if (_.isEmpty(this.props.menu))
			return null

		let header = this.props.header ? <h4>{this.props.menu.label}</h4> : null
		let nav = <Nav links={this.props.menu.links} />

		return <div>{header}{nav}</div>
	}
}

module.exports = Menu