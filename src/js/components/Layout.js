import React from 'react'
import _ from 'lodash'
import PageStore from '../stores/PageStore'
import DecodeEntities from '../modules/DecodeEntities'
import LayoutController from '../controllers/LayoutController'
import Header from './Header'
import Footer from './Footer'

// ---------------------------------------------

class Layout extends React.Component {
	constructor(props) {
		super(props)
		this.state = { data: [], page: {} }

		this._onScroll = this.handleScroll.bind(this)
		this.scroll_tm = null
	}

	componentWillMount() {
		(function tryData() {
			if (typeof window.data === 'object') {
				PageStore.Pages = window.data.pages
				this.setState({ data: _.omit(window.data, 'pages') })
				this.setPage()

				window.data = null
			}

			else setTimeout(tryData.bind(this), 100)
		}.bind(this))()

		window.addEventListener('scroll', this._onScroll, false)
	}

	componentWillUnmount() {
		window.removeEventListener('scroll', this._onScroll, false)
	}

	componentWillReceiveProps(nextProps) {
		this.setPage(nextProps.location.pathname)
	}

	componentDidUpdate() {
		this.handleSEO()

		let tmp = this.state.page
		if (tmp.template === 'template-redirect.php') {
			let redir = _.first(PageStore.Get({
				pid: tmp.id
			}))

			return browserHistory.push(redir.url.replace(/.*\/\/[^\/]*/, ''))
		}
	}

	setPage(pathname) {
		LayoutController.update(pathname || this.props.location.pathname, obj => {
			this.setState({
				page: obj
			})
		})
	}

	handleSEO() {
		if (_.isEmpty(this.state.page))
			return

		let title = this.state.page.seo.title || (this.state.page.title + ' | ' + this.state.data.site.name)
		let keywords = this.state.page.seo.keywords || ''
		let desc = this.state.page.seo.desc || ''

		document.title = DecodeEntities(title)
		document.querySelector('meta[name=keywords]').content = DecodeEntities(keywords)
		document.querySelector('meta[name=description]').content = DecodeEntities(desc)

		if (_.isFunction(window.ga))
			ga('send', 'pageview')
	}

	handleScroll() {
		clearTimeout(this.scroll_tm)
		let bodyClasses = document.body.classList

		if (!bodyClasses.contains('nohover'))
			bodyClasses.add('nohover')

		this.scroll_tm = setTimeout(() => {
			bodyClasses.remove('nohover')
		}, 350)
	}

	render() {
		if (_.isEmpty(this.state.data))
			return <div className="loader"></div>

		return (
		<div>
			<Header {...this.state.data.menus} />

			{React.cloneElement(this.props.children || <div />, {
				data: this.state.data,
				page: this.state.page,
				key: Math.random()
			})}

			<Footer {...this.state} />
		</div>
		)
	}
}

module.exports = Layout