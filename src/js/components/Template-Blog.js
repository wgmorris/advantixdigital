import React from 'react'
import { Link } from 'react-router'
import _ from 'lodash'
import offset from 'document-offset'
import classnames from 'classnames'
import If from '../modules/If'
import PageStore from '../stores/PageStore'
import Masthead from './Masthead'
import BlogNav from './BlogNav'
import CategorySelector from './CategorySelector'

// ---------------------------------------------

class BlogTemplate extends React.Component {
	constructor(props) {
		super(props)
		this.state = {}
	}

	componentWillMount() {
		let posts = _.sortBy(PageStore.Get(o => {
			if (this.props.cat == 'Blog')
				return o.type === 'post'

			else return o.type === 'post' && _.has(o.category, this.props.cat)
		}), ['id']).reverse(),
			perPage = parseInt(this.props.data.site.perpage),
			postCount = posts.length - 1,
			maxPage = Math.ceil(postCount / perPage)

		this.setState({
			posts: posts,
			_posts: posts,
			count: postCount,
			page: 0,
			perPage: perPage,
			maxPage: maxPage
		})
	}

	goToPage(p) {
		this.setState({ page: p })

		Velocity(document.body, 'stop')
		Velocity(document.body, 'scroll', {
			duration: 150,
			offset: offset(document.getElementById('the-posts')).top - 80
		})
	}

	handleChange(e) {
		let newPosts = this.state._posts

		if (e.target.value !== '')
			newPosts = _.filter(newPosts, p => {
				return _.indexOf(p.category, e.target.value) > -1
			})

		this.setState({ posts: newPosts })
	}

	shownPosts() {
		return this.state.posts.slice(
			this.state.page * this.state.perPage, 
			this.state.page + 1 * this.state.perPage
		)
	}

	render() {
		return (
		<div className="template-page template-blog template-guides">
			<Masthead {...this.props.page} />
			<BlogNav {...this.props.page} />

			<div className="page-section">
			<div className="row wrapper">
				<div className="lead center-align" dangerouslySetInnerHTML={{__html: this.props.page.fields.lead_paragraph}} />

				<If test={this.props.cat === 'Blog'}>
					<CategorySelector
						categories={this.props.data.category}
						handleChange={this.handleChange.bind(this)} />
				</If>

				<If test={_.isEmpty(this.state.posts)}>
					<p className="center-align">Sorry, no posts were found.</p>
				</If>

				<div id="the-posts" className="row page-contents" style={{float: 'none'}}>
					<If test={!_.isEmpty(this.state.posts)}>
						<div className="row">
						{this.shownPosts().map(post => {
							if (this.props.cat === 'Blog')
								return <Post key={Math.random()} {...post} />

							else return <IndustryGuide key={Math.random()} {...post} />
						})}
						</div>
						
						<If test={this.state.perPage < this.state.count}>
							<Pagination handler={this.goToPage.bind(this)} {...this.state} />
						</If>
					</If>
				</div>
			</div>
			</div>
		</div>
		)
	}
}

// ---------------------------------------------

class Pagination extends React.Component {
	constructor(props) {
		super(props)
	}

	prevPage(e) {
		return this.props.handler(this.props.page - 1)
	}

	nextPage(e) {
		return this.props.handler(this.props.page + 1)
	}

	render() {
		return (
		<div className="blog-pagination center-align">
			<If test={this.props.page > 0}>
				<a href="javascript:;" className="prev-page" onClick={this.prevPage.bind(this)}>
					<i className="material-icons">trending_flat</i>
					Previous
				</a>
			</If>

			{[...Array(this.props.maxPage)].map((x, i) => {
				return <a 
					key={Math.random()} 
					href="javascript:;" 
					className={classnames('pager', {
						active: this.props.page === i
					})}
					onClick={this.props.handler.bind(this, i)}>{i + 1}</a>
			})}

			<If test={(this.props.page + 1) < this.props.maxPage}>
				<a href="javascript:;" className="next-page" onClick={this.nextPage.bind(this)}>
					Next
					<i className="material-icons">trending_flat</i>
				</a>
			</If>
		</div>
		)
	}
}

// ---------------------------------------------

class Post extends React.Component {
	constructor(props) {
		super(props)
	}

	render() {
		return (
		<article className="post">
			<Link to={this.props.url} className="post-title" dangerouslySetInnerHTML={{__html: this.props.title}} />

			<div className="post-content">
				<span className="hide-on-small-only" dangerouslySetInnerHTML={{__html: this.props.excerpt}} />
				<Link to={this.props.url} className="read-more">Read More</Link>
			</div>

			<div className="post-author" dangerouslySetInnerHTML={{__html: this.props.author_avatar + ' by ' + this.props.author}} />
		</article>
		)
	}
}

// ---------------------------------------------

class IndustryGuide extends React.Component {
	constructor(props) {
		super(props)
	}

	render() {
		return (
		<figure className="post-guide s12 m6 l3 col">
			<Link to={this.props.url} className="cover" />
			
			<header>
				<div className="center">
					<i className="icon-tinylogo"></i>
					<p dangerouslySetInnerHTML={{__html: this.props.title}} />
				</div>
				
				<small>advantix</small>
			</header>

			<footer>
				<Link to={this.props.url}>
					Download Guide 
					<i className="material-icons">trending_flat</i>
				</Link>
			</footer>
		</figure>
		)
	}
}

// ---------------------------------------------

module.exports = BlogTemplate