import React from 'react'
import { browserHistory, Link } from 'react-router'
import _ from 'lodash'
import If from '../modules/If'
import PageStore from '../stores/PageStore'

// ---------------------------------------------

class InsightPageSlide extends React.Component {
	constructor(props) {
		super(props)
		this.state = {}
	}

	componentWillMount() {
		this.setState({
			posts: PageStore.Get({ type: 'post' }).slice(0, parseInt(this.props.maxposts) || 3)
		})
	}

	goToPost() {
		browserHistory.push(this.url)
	}

	render() {
		if (_.isEmpty(this.props) || _.isEmpty(this.state.posts))
			return null

		return (
		<div className="full-page slide-insight" data-title="Insightful">
		<div className="centerY">
			<h3>Insightful</h3>

			<div className="insight-cards">
			{this.state.posts.map(post => {
				return (
				<figure key={Math.random()} onClick={this.goToPost.bind(post)}>
					<strong dangerouslySetInnerHTML={{__html: post.title.slice(0, 1)}} />
					<aside>
						<em dangerouslySetInnerHTML={{__html: post.title}} />
						<Link to={post.url}>Read More</Link>
					</aside>
				</figure>
				)
			})}
			</div>
			
			<If test={this.props.insights_cta_url && this.props.insights_cta_text}>
				<Link to={this.props.insights_cta_url} className="btn">
					{this.props.insights_cta_text}
					<i className="material-icons">trending_flat</i>
				</Link>
			</If>
		</div>
		</div>
		)
	}
}

module.exports = InsightPageSlide