import React from 'react'
import { Link } from 'react-router'
import _ from 'lodash'
import PageStore from '../stores/PageStore'

// ---------------------------------------------

class SkilledPageSlide extends React.Component {
	constructor(props) {
		super(props)
		this.state = {}
	}

	componentWillMount() {
		let services = PageStore.Get({ type: 'service' })
		services.splice(services.length / 2, 0, {
			type: 'caption'
		})

		this.setState({ services: services })
	}

	render() {
		if (_.isEmpty(this.props) || _.isEmpty(this.state.services))
			return null

		return (
		<div className="full-page slide-skilled" ref={(c) => this.$slide = c} data-title="Skilled">
			<div className="centerY">
			<div className="row wrapper">
			{this.state.services.map(service => {
				if (service.type === 'caption')
					return (
					<div key={Math.random()} className="skilled-caption">
						<Link 
							to={this.props.services_url} 
							dangerouslySetInnerHTML={{__html: this.props.services_title}} />
					</div>
					)

				return (
				<Link key={Math.random()} to={service.url} className="circle">
					<p dangerouslySetInnerHTML={{__html: service.title}} />
				</Link>
				)
			})}
			</div>
			</div>
		</div>
		)
	}
}

module.exports = SkilledPageSlide