import React from 'react'
import { Link } from 'react-router'
import _ from 'lodash'
import PageStore from '../stores/PageStore'
import Masthead from './Masthead'
import PostsPagination from './PostsPagination'

// ---------------------------------------------

class TeamMemberTemplate extends React.Component {
	constructor(props) {
		super(props)
	}

	componentDidMount() {
		document.body.querySelector('a[href="/about/"]').classList.add('active')
		document.getElementById('footer').classList.add('no-slide')
	}

	componentWillUnmount() {
		document.body.querySelector('a[href="/about/"]').classList.remove('active')
		document.getElementById('footer').classList.remove('no-slide')
	}

	render() {
		return (
		<div className="template-page template-team template-team-member">
			<Masthead {...this.props.page} />

			<div className="page-section">
			<div className="row wrapper">
				<div className="row">
					<aside className="sidebar">
						<h2 dangerouslySetInnerHTML={{__html: 'About ' + this.props.page.title.split(' ')[0]}} />
						<hr />
					</aside>

					<div className="page-contents">
						<div dangerouslySetInnerHTML={{__html: this.props.page.content}} />
						<br /><br />

						<SocialLinks {...this.props.page.fields} />
						<Certificates {...this.props.page.fields} />
					</div>
				</div>

				<PostsPagination {...this.props.page} />
			</div>
			</div>
		</div>
		)
	}
}

// ---------------------------------------------

class SocialLinks extends React.Component {
	constructor(props) {
		super(props)
	}

	render() {
		if (_.isEmpty(this.props.social_links))
			return null

		return (
		<div className="btn-group social-links">
		{_.toArray(this.props.social_links).map(link => {
			return (
			<a key={Math.random()} href={link.url} target="_blank" className="btn btn-purple">
				{link.title}
				<i className="material-icons">trending_flat</i>
			</a>
			)
		})}
		</div>
		)
	}
}

// ---------------------------------------------

class Certificates extends React.Component {
	constructor(props) {
		super(props)
	}

	render() {
		if (_.isEmpty(this.props.certificates))
			return null

		return (
		<div className="certificates-wrapper">
			<h3>Certifications and Recognitions</h3>

			<div className="certificates table">
			{_.toArray(this.props.certificates).map(cert => {
				return (
				<div key={Math.random()} className="cell">
					<img alt={cert.title} src={cert.logo} />
				</div>
				)
			})}
			</div>
		</div>
		)
	}
}

// ---------------------------------------------

module.exports = TeamMemberTemplate