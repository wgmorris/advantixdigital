import React from 'react'
import { browserHistory, Link } from 'react-router'
import PageStore from '../stores/PageStore'
import _ from 'lodash'

// ---------------------------------------------

class PostsPagination extends React.Component {
	constructor(props) {
		super(props)
		this.types = ['post', 'team_member', 'service']
		this.state = {}
	}

	componentWillMount() {
		this.setState({
			parent: _.first(PageStore.Get({
				id: (function() {
					switch (this.props.type) {
						case 'post': return PageStore.blogID; break
						case 'team_member': return PageStore.teamID; break
						case 'service': return PageStore.servicesID; break
					}
				}.bind(this))()
			})),
			type: this.props.type,
			posts: PageStore.Get({ type: this.props.type })
		})
	}

	componentDidMount() {
		this.setState(_.merge(this.state, {
			curIndex: _.findIndex(this.state.posts, obj => {
				return obj.id === this.props.id
			})
		}))
	}

	componentDidUpdate() {
		if (!this.$prev || !this.$next)
			return

		if (this.state.curIndex === 0)
			this.$prev.classList.add('invisible')

		if (this.state.curIndex === this.state.posts.length - 1)
			this.$next.classList.add('invisible')
	}

	prevPage(e) {
		let $prev = this.state.posts[this.state.curIndex - 1]
		if ($prev) browserHistory.push($prev.url)
	}

	nextPage(e) {
		let $next = this.state.posts[this.state.curIndex + 1]
		if ($next) browserHistory.push($next.url)
	}

	scrollDown(uri, e) {
		browserHistory.push(uri)

		setTimeout(() => {
			Velocity(document.body, 'stop')
			Velocity(document.body, 'scroll', {
				duration: 700,
				offset: window.innerHeight
			})
		}, 1000)
	}

	render() {
		if (_.indexOf(this.types, this.props.type) === -1)
			return null

		return (
		<nav className="posts-pagination">
			<a href="javascript:;"
				ref={(c) => this.$prev = c}
				className="prev-page icon-larr"
				onClick={this.prevPage.bind(this)} />

			<a data-react onClick={this.scrollDown.bind(this, this.state.parent.url)} className="list-page">
				<i className="icon-grid" />
				<span>Back to {this.state.parent.title}</span>
			</a>

			<a href="javascript:;"
				ref={(c) => this.$next = c}
				className="next-page icon-rarr"
				onClick={this.nextPage.bind(this)} />
		</nav>
		)
	}
}

module.exports = PostsPagination