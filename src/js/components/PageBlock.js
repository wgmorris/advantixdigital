import React from 'react'
import _ from 'lodash'
import If from '../modules/If'
import Lead from './Lead'
import Carousel from './Carousel'
import GravityForm from './GravityForm'

// ---------------------------------------------

class PageBlock extends React.Component {
	constructor(props) {
		super(props)
		this.state = { copy: '' }
	}

	componentDidMount() {
		let scripts = /<script>((.|\n)*)<\/script>/gmi.exec(this.props.copy)
		if (!_.isEmpty(scripts))
			window.eval(scripts[1])

		let re = /(\[gravity_form.*([0-9]).*])/,
			copy = this.props.copy || this.props.content,
			form = false

		if (re.test(copy)) {
			let id = parseInt(copy.match(re)[2]) - 1
			copy = copy.replace(re, '')

			form = <GravityForm form={this.props.data.forms[id]} />
		}

		this.setState({
			copy: copy,
			form: form
		})
	}

	render() {
		return (
		<div className="page-section">
		<div className="row wrapper">
			<Lead {...this.props} />
			
			<div className="row">
				<aside className="sidebar">
					<h2 dangerouslySetInnerHTML={{__html: this.props.title}} />
					<hr />
				</aside>

				<div className="page-contents">
					<div dangerouslySetInnerHTML={{__html: this.state.copy}} />

					<If test={this.state.form}>
						{this.state.form}
					</If>
				</div>
			</div>

			<If test={this.props.fields.slide}>
				<div class="row">
					<Carousel {...this.props} />
				</div>
			</If>
		</div>
		</div>
		)
	}
}

module.exports = PageBlock