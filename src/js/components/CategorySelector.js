import React from 'react'
import _ from 'lodash'

// ---------------------------------------------

export default class CategorySelector extends React.Component {
	constructor(props) {
		super(props)
		this.state = { cat: '' }
	}

	handleChange(e) {
		this.setState({ cat: e.target.value })
		this.props.handleChange(e)
	}

	render() {
		return (
		<form action="javascript:;" className="category-selector" method="post">
			<label>
				Categories:
				<select name="category" value={this.state.cat} onChange={this.handleChange.bind(this)}>
					<option value="">All Posts</option>
					{this.props.categories.map(cat => {
						return <option key={Math.random()} value={cat.title}>{cat.title}</option>
					})}
				</select>
			</label>
		</form>
		)
	}
}