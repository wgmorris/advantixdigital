import React from 'react'
import _ from 'lodash'
import Masthead from './Masthead'

// ---------------------------------------------

class NoSidebarTemplate extends React.Component {
	constructor(props) {
		super(props)
	}

	componentDidMount() {
		document.body.classList.add('page-id-' + this.props.page.id)
	}

	componentWillUnmount() {
		document.body.classList.remove('page-id-' + this.props.page.id)
	}

	render() {
		if (_.isEmpty(this.props.page.fields))
			return null

		return (
		<div className="template-page">
			<Masthead {...this.props.page} />

			<div className="page-section">
			<div className="row wrapper skinny">
				<div className="row page-contents" dangerouslySetInnerHTML={{__html: this.props.page.content}} />
			</div>
			</div>
		</div>
		)
	}
}

module.exports = NoSidebarTemplate