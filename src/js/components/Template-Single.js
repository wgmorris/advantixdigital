import React from 'react'
import _ from 'lodash'
import Masthead from './Masthead'
import BlogBar from './BlogBar'
import PageBlock from './PageBlock'

// ---------------------------------------------

class SingleTemplate extends React.Component {
	constructor(props) {
		super(props)
	}

	componentDidMount() {
		document.body.querySelector('a[href="/blog/"]').classList.add('active')
		document.getElementById('footer').classList.add('no-slide')
	}

	componentWillUnmount() {
		document.body.querySelector('a[href="/blog/"]').classList.remove('active')
		document.getElementById('footer').classList.remove('no-slide')
	}

	render() {
		if (_.isEmpty(this.props.page))
			return null

		return (
		<div className="template-page template-single">
			<Masthead {...this.props.page} />
			<BlogBar {...this.props.page} />
			<PageBlock {...this.props.page} />
		</div>
		)
	}
}

module.exports = SingleTemplate