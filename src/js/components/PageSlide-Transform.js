import React from 'react'
import _ from 'lodash'
import InlineSVG from 'react-inlinesvg'
import detectIe from 'detectIe'
import If from '../modules/If'
import PaperDots from '../paper/PaperDots'

// ---------------------------------------------

class TransformPageSlide extends React.Component {
	constructor(props) {
		super(props)
		this.degrade = detectIe() && !Modernizr.cssanimations || /(iPad|Kindle|iPhone|Android|BlackBerry)/.test(navigator.userAgent)
	}

	handleSvgLoad(e) {
		let $circle
		if ($circle = document.body.querySelector('circle:last-child')) {
			$circle.addEventListener('animationend', () => {
				setTimeout(() => {
					document.body.querySelector('.isvg.loaded').classList.add('anim-finished')
				}, 2000)
			})
		}
	}

	render() {
		if (_.isEmpty(this.props))
			return null

		return (
		<div className="full-page slide-transform" data-title="Transform">
			<If test={!this.degrade}>
				<PaperDots count={50} />
			</If>

			<div className="centerY">
				<If test={this.degrade}>
					<img src="/img/transform.png" className="full-width" style={{'max-width': '531px'}} />
				</If>

				<If test={!this.degrade}>
					<InlineSVG
						src="/img/transform2.svg"
						className="hide-on-small-only"
						onLoad={this.handleSvgLoad.bind(this)} />

					<img src="/img/transform.svg" className="hide-on-med-and-up full-width" />
				</If>

				<h1 dangerouslySetInnerHTML={{__html: this.props.transform_title }} />
			</div>

			<a
				className="scroll-down-anchor"
				onClick={this.props.clickHandler.bind(this)} />
		</div>
		)
	}
}

module.exports = TransformPageSlide