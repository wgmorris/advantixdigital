import React from 'react'
import { Link } from 'react-router'
import _ from 'lodash'
import PageStore from '../stores/PageStore'

// ---------------------------------------------

class BlogBar extends React.Component {
	constructor(props) {
		super(props)
	}

	componentDidMount() {
		addthis.update('share', 'title', this.props.title)
		addthis.update('share', 'url', location.href)
		addthis.update('share', 'description', document.querySelector('meta[name=description]').content)
		addthis.toolbox('.addthis_toolbox')
	}

	render() {
		return (
		<div className="blog-bar">
		<div className="row wrapper">
			<aside className="sidebar">
				<Link to={PageStore.getUrl(PageStore.blogID)}>
					<i className="material-icons flipX">trending_flat</i>
					Back to news list
				</Link>
			</aside>

			<div className="page-contents right-align addthis_toolbox">
			<ul>
				<li>Share on:</li>

				<li>
					<a href="javascript:;" className="addthis_button_facebook">
						<i className="icon-fb" />
					</a>
				</li>

				<li>
					<a href="javascript:;" className="addthis_button_google_plusone_share">
						<i className="icon-gplus" />
					</a>
				</li>

				<li>
					<a href="javascript:;" className="addthis_button_twitter">
						<i className="icon-twtr" />
					</a>
				</li>

				<li>
					<a href="javascript:;" className="addthis_button_linkedin">
						<i className="icon-lnkd" />
					</a>
				</li>

				<li>
					<a href="javascript:;" className="addthis_button_buffer">
						<i className="icon-buffer" />
					</a>
				</li>

				<li>
					<a href="javascript:;" className="addthis_button_email">
						<i className="icon-email" />
					</a>
				</li>
			</ul>
			</div>
		</div>
		</div>
		)
	}
}

module.exports = BlogBar