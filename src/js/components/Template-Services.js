import React from 'react'
import { browserHistory } from 'react-router'
import _ from 'lodash'
import If from '../modules/If'
import Masthead from './Masthead'
import Carousel from './Carousel'
import PageStore from '../stores/PageStore'

// ---------------------------------------------

class ListTemplate extends React.Component {
	constructor(props) {
		super(props)
		this.state = {}
	}

	componentWillMount() {
		if (!_.isEmpty(this.props.page.fields.expertise_items))
			this.setState({
				expertiseItems: this.props.page.fields.expertise_items
			})
	}

	componentDidMount() {
		document.getElementById('footer').classList.add('no-slide')
	}

	componentWillUnmount() {
		document.getElementById('footer').classList.remove('no-slide')
	}

	render() {
		return (
		<div className="template-page template-list">
			<Masthead {...this.props.page} />

			<div className="page-section">
			<div className="row wrapper center-align page-contents services-list">

			{PageStore.Get({
				type: 'service'
			}).map(service => {
				return <Service key={Math.random()} {...service} />
			})}
			</div>
			</div>
			
			<If test={this.state.expertiseItems}>
				<div className="page-section no-top-padding">
				<div className="row wrapper center-align page-contents expertise-list">
					<h2 dangerouslySetInnerHTML={{__html: this.props.page.fields.end_title}} />
					<hr />

					<span dangerouslySetInnerHTML={{__html: this.props.page.fields.end_copy}} />

					<div className="row">
					{_.chunk(
						this.state.expertiseItems, 
						Math.floor(this.state.expertiseItems.length / 2)
					).map(itemsGroup => {
						return (
						<div key={Math.random()} className="s12 m6 col">
						{itemsGroup.map(item => {
							return <span 
								key={Math.random()} 
								className="expertise-item" 
								dangerouslySetInnerHTML={{__html: item.title}} />
						})}
						</div>
						)
					})}
					</div>
				</div>
				</div>
			</If>
		</div>
		)
	}
}

// ---------------------------------------------

class Service extends React.Component {
	constructor(props) {
		super(props)
	}

	handleClick(e) {
		browserHistory.push(this.props.url)
	}

	render() {
		return (
		<div className="s12 m4 col" onClick={this.handleClick.bind(this)}>
		<figure className="circle">
			<figcaption className="table cover">
			<div className="cell">
				<img src={this.props.fields.icon} />
				<h2 dangerouslySetInnerHTML={{__html: this.props.title}} />
			</div>
			</figcaption>

			<div className="hover-circ">
			<figcaption className="table cover">
				<div className="cell" dangerouslySetInnerHTML={{__html: this.props.fields.caption || ''}} />
			</figcaption>
			</div>
		</figure>
		</div>
		)
	}
}

// ---------------------------------------------

module.exports = ListTemplate