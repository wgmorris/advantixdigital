import React from 'react'
import _ from 'lodash'
import CaptureClicks from '../modules/CaptureClicks'

import Page from './Template-Page'
import Blog from './Template-Blog'
import Single from './Template-Single'
import Gated from './Template-Gated'
import About from './Template-About'
import Contact from './Template-Contact'
import Clients from './Template-Clients'
import Services from './Template-Services'
import Nosidebar from './Template-NoSidebar'
import Team from './Template-Team'
import TeamMember from './Template-TeamMember'

const Templates = { 
	About, Clients,
	Services, Team,
	Contact, Gated,
	Nosidebar
}

// ---------------------------------------------

class TemplateHandler extends React.Component {
	constructor(props) {
		super(props)
	}

	render() {
		let contents = (() => {
			if (_.isEmpty(this.props.page))
				return null

			let tmpl = this.props.page.template

			if (this.props.page.is_blog)
				return <Blog cat="Blog" {...this.props} />

			else if (tmpl === 'template-insights.php')
				return <Blog cat="Industry Guides" {...this.props} />

			else if (this.props.page.type === 'post') {
				if (this.props.page.fields.is_gated)
					return <Gated {...this.props} />
				
				return <Single {...this.props} />
			}

			else if (this.props.page.type == 'team_member')
				return <TeamMember {...this.props} />

			else if (tmpl) {
				const Dummy = Templates[
					_.upperFirst(
						tmpl.match(/template\-([A-z]+)\./)[1]
					)
				]

				return <Dummy {...this.props} />
			}

			return <Page {...this.props} />
		})()

		return <CaptureClicks>{contents}</CaptureClicks>
	}
}

module.exports = TemplateHandler