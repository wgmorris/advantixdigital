import React from 'react'
import raf from 'raf'
import _ from 'lodash'
import GravityForm from './GravityForm'

// ---------------------------------------------

class TrySomethingNew extends React.Component {
	constructor(props) {
		super(props)
	}

	componentDidUpdate() {
		this.$h2 = document.querySelector('.p2 h2')
		this.$label = document.querySelector('.p2 textarea + label')

		if (this.$footer)
			this.$footer.classList.remove('form-in-view')
	}

	componentDidMount() {
		this.$footer = document.getElementById('footer')
		this.$p1 = document.querySelector('.page.p1')
		this.$p2 = document.querySelector('.page.p2')
	}

	slideInForm() {
		this.$footer.classList.add('form-in-view')
		this.reposition()
	}

	slideOutForm() {
		this.$footer.classList.remove('form-in-view')
		this.reposition()
	}

	setCopy(bool) {
		if (bool) {
			this.$h2.innerHTML = 'Great! Let\'s start the transformation today'
			this.$label.innerHTML = 'Comment'
		}

		else {
			this.$h2.innerHTML = 'Okay. Change can be scary. Let\'s do lunch first and go from there.'
			this.$label.innerHTML = 'Favourite Food'
		}

		this.slideInForm()
	}

	reposition() {
		if (window.innerWidth < 786) return

		Velocity(document.body, 'stop')
		Velocity(document.body, 'scroll', {
			duration: 650,
			offset: document.body.scrollHeight
		})
	}

	render() {
		return (
		<div className="full-page slide-ready" data-title="Ready?">
			<a href="javascript:;" className="carousel-link" onClick={this.slideOutForm.bind(this)}>
				<i className="icon-larr"></i>
			</a>

			<div className="carousel">
				<div className="page p1">
				<div className="center-align row wrapper">
					<h2>Try something new?</h2>

					<div className="btn-group">
						<a href="javascript:;" className="btn" onClick={this.setCopy.bind(this, true)}>
							Yes
						</a>

						<a href="javascript:;" className="btn faded" onClick={this.setCopy.bind(this, false)}>
							No
						</a>
					</div>
				</div>
				</div>

				<div className="page p2">
				<div className="center-align row wrapper">
					<h2 onClick={this.slideOutForm.bind(this)}>Great! Let's start the transformation today</h2>
					<GravityForm form={this.props.data.forms[0]} />
				</div>
				</div>
			</div>

			<a href="javascript:;" className="carousel-link" onClick={this.slideInForm.bind(this)}>
				<i className="icon-rarr"></i>
			</a>
		</div>
		)
	}
}

module.exports = TrySomethingNew